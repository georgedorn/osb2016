- Usually a request to add a feature or improve speed/reliability
-- usually a reasonable request from the outside

- when to refactor vs rewrite?
-- developers working on it know more about the internals
--- code smells
--- no changes in years

options:
- small change
-- can make a small change without a lot of analysis or refactoring
-- but if the smelly code will be high-traffic in the future
- refactor to make a change
-- SOLID mnemonic about OOP
-- do this for high-traffic areas to avoid making the mess worse as later changes follow the same pattern
-- testing probably necessary
- rewrite
-- different from just refactoring
--- refactoring leaves the API unchanged
--- refactoring only changes internal code
--- refactoring relatively safe, small incremental changes where things still work each step
-- rewriting throws away the original code completely
-- rewrites can be dangerous, can miss requirements in original
-- rewrites take a lot of time, allowing project to be cut or requirements to change during rewrite

refactoring is a lot less painful than rewriting
- check assumptions when desiring to rewrite
- check with stakeholders

refactor vs rewrite decision factors:
- Goals
-- just add a feature?
-- bugfixing
-- improving performance
-- updating dependency
-- securty updates
- Risks
-- what happens if things go wrong?
-- what if you miss features?
-- how much loss of accuracy is permissable?
-- loss of reputation?
- Tests
-- how much automated testing?
-- does anybody know the codebase well?
-- if there are no experts or insufficient testing, rewrite is even more risky
-- how complete are requirements?
-- will updating a dependency break tests?

- case study about search form results (doctor appt scheduler)
-- legacy code builds numerous SQL statements that get increasingly complex as new rules are added
-- replace some SQL with ElasticSearch (rewrite?)
-- reliability and accuracy very important (risks)
-- nobody had comprehensive list of business rules?
-- losing requirements is very risky

Collect info about all three factors before making a decision.


How to rewite:
- need everybody on-board, including non-tech team members
-- stakeholders need to understand nothing new created during rewrite
- stabilize codebase before rewriting
-- don't extend before fixing bugs
-- github scientist rubygem (behavior analysis)
-- feature flags to run both codebases at the same time
-- beta users (high-traffic users who know features)
--- multiple points of contact, particularly in other timezones
- test suite
-- behavior definitions
--- pass the business rules around to get everybody to sign off on known definitions
-- test suite should run on both legacy and rewrite

Design patterns to help rewrite, inside same codebase:
- strangler application (martin fowler)
-- slowly write new system around old system until old system is strangled and removable
- sprout methos (michael feathers 'working with legacy code')
-- create new good code at a point of entry
-- use feature flag to conditionally call a replacement method
- wrap method (also feathers)
-- wrap old code in new class, use feature flag to conditionally use legacy class

wrap vs sprout:
- depends on analysis requirements and testing interface.
- similar approaches anyway

See Gang of Four design patterns + Feathers book.
