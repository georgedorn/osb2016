Context:
- state of georgia websites
-- mostly drupal
-- assistance to state agencies to advocate for users over commissioners

Most accessibility is nearly invisible to most users.
- we're prolly the choir, but how to make case to stakeholders

Why accessibility:
- many kinds of disabilities
-- visual: blind, color blind, low vision
-- auditory
-- motor
-- cognitive
-- seizures
-- fatigue
- Carrot:
-- SEO and accessibility overlaps
--- SE doesn't see images or hear audio
--- Google says use Lynx to see what googlebot sees.
--- Google = Screenreader
-- Accessibility = User-Friendly
-- Users w/ Disabilities are large audience
--- 15% of users have some disability
--- 4% of men have color-blindness
--- 50% of oldest, fastest-growing population has low vision
-- Everybody will have a disability eventually
--- Everybody is temporarily-abled
- Stick:
-- lawsuits
-- DoJ settlements use WCAG 2.0 level AA guidelines
--- newer than ADA
--- long list of suits:
---- edX
---- Carnival Cruise
---- SAM.gov
---- Seattle Public Schools

Georgia state gov:
- central UX management site
-- other agencies manage content, not UX

508 e.g.s:
- no frames
- no flash
- label images
- label tabular data

More from GATech:
- audit 13 templates x 33 page types
-- iterative approach to meet WCAG
- link text standards
- tabbing visibility
- semantic html

Visual testing:
contrast every element against every theme
- chrome tools:
-- fontface ninja
-- colorzilla
- webaim color contrast checker
- spreadsheet of every text/font/size vs theme
-- find failures, standardize
--- fix color contrast and text legibility

But all of this was post-hoc.
- had some acceptance testing, but...
- things they didn't know without consult
- much easier to do as part of workflow than after work is done


What to do:
- A11Y Project Checklist - a11yproject.com (also good resource for a11y widgets)
- color
-- need 4.5 : 1 color contrast ratio
-- webaim contrast checker
-- contrast ratio leaverou
-- color testing
-- nocoffee colorblindness checker
-- could fix this whole problem by building color checking into tools for design/CMS
- text
-- 1em or bigger and use ems
-- 1.2em-1.6em line height
-- check if layout breaks at 200%
--- responsive can help here
-- no all caps
--- breaks screenreaders
--- harder to read for sighted viewers
- touch targets
-- make as big as possible for layout
-- at least 9mm x 9mm, with inactive space
- images
-- alt text for any image that adds value / context
-- null alt-text for decorative (blank alt text, not missing, as SRs sometimes read filenames)
-- see w3.org WAI alt decision tree
-- build the alt-text fields into CMS, offer advice about how to use / when
- provide hidden text for glyphs
-- visibility: hidden or display: none hide from SRs
-- use position:absolute to push off page, w/ backups
- semantic markup
-- use right tag for right purpose
-- not div for button
-- not blockquote for non-quotes
-- h1-h6 for actual headings
--- use in right order, h1 through h6
---- SRs use these for gopher-esque trees
--- maybe don't use h1 for agency name for every internal page
--- when building tools, make h1-h6 make logical sense even w/ template inclusion!
- links:
-- use a skip-to-content anchor before nav, hidden until tab-selected
-- no target=_blank, confuses SRs and breaks back button
-- don't remove :focus outline
--- use ally.js to force-add them back in
-- no 'read more' links, descibe every link accurately
--- unless appropriate, in which case use WAI-ARIA tags (aria-label, aria-labelledby)
---- ARIA is used by many SRs and Googlebot
- ARIA
-- landmark 'role' attribute
--- label inputs w/ purpose
- forms
-- a label for every field
--- help text between label and input
-- label required w/ *
-- errors via text, not just color
-- check tab order
- tables
-- use for tabular data
-- thead for head, th instead of td
-- caption table for explanation
- JS
-- most SRs can handle JS
--- but don't assume it is enabled
-- enhance, not require
-- see ally.js for tools


