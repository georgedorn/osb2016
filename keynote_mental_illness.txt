see modelviewculture - Pressure of Success in undergrad CS

intimidation getting into OSS contributing exacerbates some illness issues
- or compounds them

'open source' means more than software
- multidisciplinary and diverse approaches needed

lack success at side projects can feed insecurities and self-worth issues
- underpaid issues

created IFMe, open-source community for discussion of mental health issues

communication strategies for remote oss teams:
- google hangouts for f2f meeting new contributors
- slack, irc, keep non-tech contributors in mind
- code of conduct / contributor covenant
- docs for all OS
- get everybody in on CRs, even non-tech
- give credit to contributors
-- beyond commits
-- e.g. contributors file
-- or IFMe's contrib page

see:
@thebrainly
@osmihelp (finkler)
@mhprompt
@ifmeorg
if-me.org





