most IoT is fragmented
- Nest has one platform
- fitbit has own platform
-- not much interop w/o hacks

poor standards and war for attention:
- lots of dev boards
-- pi vs qube vs next
- many cloud services

everything competing for time
- even trying to choose a device has a cost
-- and picking one locks you into a vendor
- buying a device = handing over your data to a cloud provider

spectrum of maker vs consumer:
- pi and arduino, full control but costs time
- consumer devices limit data control, feature control but are polished
-- similar to OSS vs closed software
- maker projects that grow into kickstarter projects
-- but many crowdfunding projects turn into consumer services
-- and have other risks like acqui-killing (see Revolver killed by Google)

ignore IoT hype:
- consider problems to be solved and resources available
-- sensors, data analysis
- lots of obsolete devices already have many sensors
-- more than a Pi


case studies:
- basic problems like security, shelter, etc
-- power off at home?
-- phone charged constantly - query battery about whether being charged
--- sms or data if power out
-- home security
--- listen for bluetooth
--- accelerometer motion in phone
--- listen for new wifi devices for new networks
-- monitor grandma for health
--- simple motion detection on camera feed
--- set warning if no motion after e.g. 9am
-- monitor home internet connection
--- send SMS or data when internet not available

examples need context:
- need a phone connected to wifi able to make web requests
-- no special apps, web apis where possible
-- web api can often get at phone sensors
--- control own data

web-available apis,  especially firefox
physical:
- accelerometer
- orientation
- proximity
- ambient light
- power / battery
media:
- microphone - detect when abnormal noise happens
- camera
-- crazy useful
-- motion detection
-- augmented vr in webvr
- speech recog
- speech synth
- sound output
Radio:
- bluetooth
- nfc
- wifi
- gps
network apis:
- web stuff
- dlna
- mdns / bonjour

android:
- probably don't need a native app

interactions:
- web push notifications
- sms
- slack / irc / etc
- iftt maker channel
-- but yet another closed platform?

github.com/rabimba/context

big drive by mozilla for open web standards for all lf this



