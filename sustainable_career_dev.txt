-change from community manager to dev suddenly meant more presssure

- pressure in industry to do more work outside work
-- meetups
-- side projects
-- other career building
- problem is systemic to the industry
-- not a lot of data on the subject
-- needed to do original research
- we assume these things are necessary for career advancement

59% felt pressure vs 34% didn't
- pressure may go down with time

reasons:
- keep up w/ orkers
- keep up w/ industry
- outside learning opportunities (e.g. crossing into other domains)
-- or build skills not avail at work
- general curiosity

why bad:
- lowers diversity due to time constraints
-- parents, ppl with disabilities
- burnout
-- mental health
-- e.g. keep up with orkers leads to workaholism, alcoholism, etc
- discourages people
-- amazing devs do side work, terrible devs don't
--- JKM callout

what causes this:
- 'passion'
-- waaaay overused
-- moderately enthusiastic programmer blog post
-- e.g. Uber listing
-- passion = codeword for exploiting lack of boundaries
- side project culture
-- terry burns on mvculture
- github as resume
-- poor substitute
-- lacks non-tech skill demonstration
--- non-tech skills vital for good dev

Translate causes into:
- motivation/curious
- adaptable
- committed 

ideas:
- kzou's guide to learning
-- pick one thing and measure progress
- don't do everything at once
-- couch25k example
- figure out what you want and set boundaries
-- holistic whole-life choice
- figure out your learning style
-- solo vs groups vs being mentored
-- learn by writing docs
-- find domain mentors for each area
-- be a mentor, which enhancing own understanding
- say no to things
- schedule time for goals
-- at work!
- move outside work activities to work
- 'slow pairing'
-- deep dive into topic, not for specific bugfixes
- celebrate progress
-- tokens
-- journaling
- peptalk generator
- enjoy hobbies w/o guilt
-- b/c they make you happy + work better

@elnoelle
