http://bit.ly/OSB16-machinelearning101
CNNs are good for:
- spatial relationships of data
-- image classifier
-- music classification (spotify)
-- image localization sans geo-tag (PlaNet from Google)

CNN vs NN:
- trying to determine filters to classify data
-- instead of just forward

Inputs:
- scan a small region
- calc RGB inputs from each region (channels)
-- often first layer develops edge detector
- downsample to reduce inputs repeatedly to get to 1x1 with many outputs, feed those into regular NN

Pooling:
- subsample image after each layer
-- e.g. after first layer, downsample to 1/2

This in theano:
theano.tensor (uses GPUs as well)
- well established
- ecosystem

TensorFlow used for AlphaGo
- performance getting better
- see tflearn

Keras is front-end for both theano + TF.

Image Prep:
- start with a curated image set
-- otherwise will need to:
--- normalize image to same size
---- squeeze or resize/crop
--- convert image to numpy array
--- save converted images with numpy's compressor

Keras model creation:
- declarative model
- many layer types
- add compile parameters (loss fn, e.g.)
- can hook into signals during training
-- e.g. ModelCheckpoint whenever model improves
-- EarlyStopping if conditions met
-- TensorBoard logging
- rectified linear unit tends to be faster and better than sigmoid for this

CNNs can be parallelized:
- each layer can happen in parallel
- nodes aren't update until end of epoch
- parallel pipelines in a GPU is useful
- parallel systems also good
- CUDA installation can be painful
-- mostly nVidia?
- Google licensing out TPUs
- Keras good at reporting whether GPU/CPU in use

After model complete:
- export model weights
- can import the model into a new identical Keras model definition
- add outputs for classification (cat vs dog)


Can persist to several backends and reinstate as needed.

Advice:
- don't make model more complex than input data

