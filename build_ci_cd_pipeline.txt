Changing from slow release cycle to CI/CD.  (example from pivotal)

What to do when deploy fails:
- read logs, don't understand messages
- reboot, but server doesn't come back up
- everything works locally

Wrong:
- didn't test on server env like prod
- didn't deploy for six months, so changes acumulated
- prod is a snowflake you need to manage via directly SSHing into box
-- how to automate deploying new servers?
- manual deployment
-- imagine manually deploying to 50 servers
- testing not enforced after every commit/push

Pre-reqs:
- need tests, preferably comprehensive
- need to deploy often.  deploy regularly or after every change.
- replace prod and manual deploys with 'cloud platform'
-- IaaS like AWS, Azure, etc.
-- PaaS like heroku, cloud foundary, beanstalk (aws)

Lifecycle:
- dev commits regularly
- CI/CD server runs tests automatically after changes to repo
- CD auto-deploys if green

More valuable tests also mean more expensive:
- Unit is cheap and fast
- end-to-end is expensive and fragile
- smoke tests are hard to debug and expensive but test things like a user on prod-like
- need a good mix of tests to be confident of CI/CD

Setting up CI via Travis
- example Go app: github.com/jadekler/git-cicd-talk
-- helloworld http server w/ one test
-- enable project in travis interface
-- .travis.yml defines how to run tests and how to deploy when build passing

Examples from complex to simple:
- Concourse (self-hosted) 
- Jenkins (self-hosted) but pipeline is defined in UI
- GoCD
- CircleCI
- TravisCI - pipeline defined in .yml

Concourse:
- can define multiple starting points and conditionally run each stage and each service
- run each service's tests individually, then end-to-end, then deploy
- pipeline.yml
-- define resources needed (git)
-- define jobs (tests/deploy)
- doesn't run branch commits unless defined in pipeline.yml
- can do version branches / tags with a separate pipeline for each

Jenkins 2.0 has pipeline features as well.  
- Can wrap Jenkins via chef/etc to avoid snowflake Jenkins instances.

